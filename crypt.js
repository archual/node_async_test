"use strict";
const crypto = require('crypto');

const secret = 'abcdefg';

// Async iteration, while not finished.
const iteration = function(data) {
    let x = 0;
    let results = [];
    const int = setInterval(() => {
        let coded = [];
        coded[0] = toQR(data[x], 'we');
        coded[1] = toQR(data[x], 'sd');
        coded[2] = toQR(data[x], '2');
        coded[3] = toQR(data[x], '3');
        coded[4] = toQR(data[x], '4');
        results.push(coded);

        if (x % 1000 === 0) {
            console.log('1000 results: ', results);
            results = [];
        }

        x++;

        if (x >= data.length) {
            clearInterval(int);
        }
    }, 0);
};

function toQR(line, modification) {
    const hash = crypto.createHmac('sha256', secret)
        .update(line + modification)
        .digest('hex');
    return hash;
}

// Generate fake data.
function generateFakeData(amount) {
    let data = new Array(amount);
    for (let i = 0; i < amount; i++) {
        data[i] = Math.random();
    }
    return data;
}

const fakeData = generateFakeData(30000);

iteration(fakeData);
console.log('test async');
setTimeout(() => console.log('test async 500'), 500);
setTimeout(() => console.log('test async 1000'), 1000);
setTimeout(() => console.log('test async 2000'), 2000);
setTimeout(() => console.log('test async 5000'), 5000);