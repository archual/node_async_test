"use strict";

// Run async animation
function setAnimation(time) {
    return new Promise((resolve, reject) => {
        const interval = animation();

        setTimeout(() => {
            clearInterval(interval);
            resolve('Done');
        }, time);
    });
}

const animation = function() {
    const P = ["\\", "|", "/", "-"];
    let x = 0;
    return setInterval(function() {
        process.stdout.write("\r" + P[x++]);
        x &= 3;
    }, 250);
};

async function animate(time) {
    try {
        // show animation
        const result = await setAnimation(time);
        console.log('Анимация завершена');
    }
    catch (e) {
        console.log('Что-то пошло не так: ', e);
    }
}

const result = animate(4000);
setTimeout(() => console.log('Код анимации не синхронный'), 1000);