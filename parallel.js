"use strict";

const http = require('http');

const reqOptions = {
    hostname: 'www.googl.com'
};

const reqOptions2 = {
    hostname: 'www.google.com'
};

twoRequests(reqOptions, reqOptions2);

async function twoRequests(reqOptions, reqOptions2) {
    try {
        const results = await Promise.all([getRequest(reqOptions), getRequest(reqOptions2)]);
        console.log('Сумма результатов (первые 20 символов от каждого запроса): ', results.reduce((prev, curr) => {
            return prev + curr.substr(0, 10);
        }, ''));
    }
    catch (e) {
        console.log('Что-то пошло не так: ', e);
    }
}

function getRequest(reqOptions, message) {
    return new Promise((resolve, reject) => {
        console.log('request started', message);
        const req = http.request(reqOptions, (res) => {
            console.log('request success');
            let body = '';
            res.on('data', (chunk) => {
                body += chunk;
            });
            res.on('end', () => {
                resolve(body);
            });
        });
        req.on('error', (e) => {
            console.log('request failed');
            reject(e);
        });
        req.end();
    });
}
