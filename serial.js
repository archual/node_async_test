"use strict";

const http = require('http');

const reqOptions = {
    hostname: 'www.googl.com'
};

const reqOptions2 = {
    hostname: 'google.com'
};

twoRequests(reqOptions, reqOptions2);

async function twoRequests(reqOptions, reqOptions2) {
    try {
        const result = await getRequest(reqOptions);
        await getRequest(reqOptions2, result);
        console.log('Я молодец, я отработал');
    }
    catch (e) {
        console.log('Что-то пошло не так: ', e);
    }
}

function getRequest(reqOptions, message) {
    return new Promise((resolve, reject) => {
        console.log('request started', message);
        const req = http.request(reqOptions, (res) => {
            console.log('request success');
            let body = '';
            res.on('data', (chunk) => {
                body += chunk;
            });
            res.on('end', () => {
                resolve(body);
            });
        });
        req.on('error', (e) => {
            console.log('request failed');
            reject(e);
        });
        req.end();
    });
}
